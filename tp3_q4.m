

% Question 3
syms c_4 s_4 c_5 s_5 c_6 s_6

R_a_theta4 = [[c_4, -s_4, 0];
              [s_4,  c_4, 0];
              [0,      0, 1]]

R_b_theta5 = [[1, 0, 0];
              [0, c_5, -s_5];
              [0, s_5, c_5]];

R_c_theta6 = [[c_6, -s_6, 0];
              [s_6,  c_6, 0];
              [0,      0, 1]]

R_4to6 = R_a_theta4 * R_b_theta5 * R_c_theta6


matrix2latex_syms(R_a_theta4)
matrix2latex_syms(R_b_theta5)
matrix2latex_syms(R_c_theta6)
matrix2latex_syms(R_4to6)