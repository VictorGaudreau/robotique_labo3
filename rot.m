function T = rot(Q, omega, theta )

  w_X = [0          -omega(3)    omega(2);
         omega(3)   0           -omega(1);
        -omega(2)   omega(1)    0];

  %R_ws_theta = eye(3) + sin(theta).*w_X + (1-cos(theta)).*(w_X^2);
  R_ws_theta = expm( theta * w_X );

  OQ_s = Q;

  T = zeros(4);
  T(1:3, 1:3) = R_ws_theta;
  T(1:3, 4) = (eye(3)- R_ws_theta) * OQ_s;
  T(4, 1:4) = [0,0,0,1];

end

