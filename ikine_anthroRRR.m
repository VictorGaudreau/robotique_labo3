function theta= ikine_anthroRRR(Wdes,soltype)

constants

% Infos :  d = sqrt ( d(C,W)^2 )
%          d^2 = d(O',W)^2 - d(O',C)^2
%          where O' = d1
d = sqrt(Wdes(1)^2 + Wdes(2)^2 + (Wdes(3)-l1)^2 - (d1^2));
%d = sqrt(sum(Wdes.^2) - d1^2);
%if d >= (l2+l3) 
%  disp("!! error in calculation of d !!")
%  return
%end

dz = Wdes(3) - l1;
alpha = atan2(dz, sqrt(Wdes(1)^2 + Wdes(2)^2 - d1^2));
%alpha = atan2(Wdes(3), sqrt(Wdes(1)^2 + Wdes(2)^2 - d1^2));

if soltype(1) == "r" % Right shoulder
	theta(1) = atan2(Wdes(2), Wdes(1)) + asin(d1/sqrt(Wdes(1)^2 + Wdes(2)^2));

	if soltype(2) == "d" % Elbow down
		theta(2) = alpha - acos((l2^2 + d^2 - l3^2)/(2*d*l2)) - pi/2;
		theta(3) = -acos((d^2-l2^2-l3^2) / (2*l2*l3));

	elseif soltype(2) == "u" % Elbow up
		theta(2) = alpha + acos((l2^2 + d^2 - l3^2)/(2*d*l2)) - pi/2;
		theta(3) = acos((d^2-l2^2-l3^2)/(2*l2*l3));
	else
    disp("invalid soltype(2)");
  end

elseif  soltype(1) == "l" % Left shoulder
	theta(1) = atan2(Wdes(2), Wdes(1)) - asin(d1/sqrt(Wdes(1)^2 + Wdes(2)^2)) + pi;

	if soltype(2) == "d" % Elbow down
		theta(2) = pi - alpha - acos((l2^2 + d^2 - l3^2)/(2*d*l2)) - pi/2;
		theta(3) = -acos((d^2 - l2^2 - l3^2)/(2*l2*l3));

	elseif soltype(2) == "u" % Elbow up
		theta(2) = pi - alpha + acos((l2^2 + d^2 - l3^2)/(2*d*l2)) - pi/2;
		theta(3) = acos((d^2 - l2^2 - l3^2)/(2*l2*l3));
	else
    disp("invalid soltype(2)");
	end

else
  disp("invalid soltype(1)");
end

  % Wrapping angles between ]-pi,pi]
  for i = 1:3
    theta(i) = wrapangle(theta(i));
  end

  % Check validity
  for i = 1:3;
    if iscomplex( theta(i) )
      warning('Invalid position requested. Function ikine_anthroRRR()');
    endif
  end

end

