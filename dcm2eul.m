function angles = dcm2eul(R, solnumber)
%DCM2EUL Summary of this function goes here
%   Detailed explanation goes here

angles = zeros(1,3); 
  % 1 = theta4 = phi
  % 2 = theta5 = theta
  % 3 = theta6 = psi

  % Check validity
  for i = 1:3;
    if iscomplex( R )
      warning('Invalid R matrix. Function dcm2eul()');
    endif
  end


try

  if ( R(3,1) == 0 && R(3,2) == 0) % s5 = 0 means theta5 = theta = 0 or pi
    
    if R(3,3) == 1 % for theta = 0
      angle(1) = 0; % Let phi = 0.
      angle(2) = 0;
      angle(3) = atan2( R(1,2), R(1,1) );

    elseif R(3,3) == -1 % for theta = pi
      angle(1) = 0; % Let phi = 0.
      angle(2) = pi;
      angle(3) = atan2( -R(1,2), R(1,1) );
    end

  else 

    if solnumber == 0 % for theta between ]0 to pi[
      angles(1) = atan2( R(1,3), -R(2,3) );
      angles(2) = acos( R(3,3) );
      angles(3) = atan2( R(3,1), R(3,2) );

    elseif solnumber == 1 % for theta between ]0 to -pi[
      angles(1) = atan2( -R(1,3), R(2,3) );
      angles(2) = -acos( R(3,3) );
      angles(3) = atan2( -R(3,1), -R(3,2) );

    else 
      disp("invalid solnumber");
    end

  end

  % Wrapping angles between ]-pi,pi]
  for i = 1:3;
    angles(i) = wrapangle(angles(i));
  end

catch
  warning('Problem using function dcm2eul()');
end

end
