function Ts = fkine_gen3lite(q_list)

% Load useful constants
constants

%% Question 2 -- repos 

dx = 0; 
dy = d1;
dz = l1 + l2 + l3 + dwe;
T_repos = [1,0,0,dx; 
           0,1,0,dy; 
           0,0,1,dz; 
           0,0,0,1];
       
%% Question 3 
nconfigs = size(q_list,1);

Ts = zeros(4,4,nconfigs);

for k = 1:nconfigs
  q = q_list(k,:);
  %q = zeros(1,6);

  T_1s    = rot( [0;     0; 0],     z_s, q(1));
  T_2s    = rot( [0;   -d1; l1],   -y_s, q(2));
  T_3s    = rot( [0;   -d1; l1+l2], y_s, q(3));
  T_4s    = rot( [0;   -d1; l1+l2], z_s, q(4));
  T_5s    = rot( [0;   -d1; l1+l2+l3],  x_s, q(5));
  T_6s    = rot( [dwo; -d1; l1+l2+l3],  z_s, q(6));
  T_tools = rot( [dwo; -d1; l1+l2+l3+dwe], z_s, 0);

  T_se_q = T_1s * T_2s * T_3s * T_4s * T_5s * T_6s * T_tools * T_repos;

  Ts(:,:,k) = T_se_q;
end

end

