% Quelques tests pour le TP3
% Utilisez ces tests pour vous guider, et aussi pour vérifier les noms et
% prototypes de vos fonctions

constants

%% dcm2eul
R = [0.790482, 0.0117385, 0.612372;
     0.48368, -0.625343, -0.612372;
     0.375754, 0.780262,   -0.5];

angles = dcm2eul(R,0);
assert(size(angles,1) == 1 && size(angles,2) == 3);
phi = angles(1); 
theta = angles(2); 
psi = angles(3);
erreurangles1 = abs(exp(1i*(phi-pi/4))-1)+abs(exp(1i*(theta-2*pi/3))-1)+...
                abs(exp(1i*(psi-pi/7))-1)

angles = dcm2eul(R,1);
phi = angles(1); theta = angles(2); psi = angles(3);
erreurangles2 = abs(exp(1i*(phi-5*pi/4))-1)+abs(exp(1i*(theta+2*pi/3))-1)+...
                abs(exp(1i*(psi-8*pi/7))-1)


%% ikine_anthroRRR
z_max = l1+l2+l3; % = 768.3
Wdes = [0,-10,z_max];  % position haute
thetas = ikine_anthroRRR(Wdes,"ru");
disp("--------------------")
disp("1: Sol ru:")
disp(thetas);

Wdes = [525,-10,243.3];  % position devant
thetas = ikine_anthroRRR(Wdes,"ru");
disp("2: Sol ru:")
disp(thetas);

% Pour valider la cohérence de votre nomenclature ru, rd, lu, ld
Wdes = [450,0,250];  % point générique
thetas = ikine_anthroRRR(Wdes,"ru");  
% cette configuration doit avoir theta1 proche de 0, theta3 > 0 (épaule droite, coude haut)
disp("3: Sol ru:")
disp(thetas);
% cette configuration doit avoir theta1 proche de 0, theta3 < 0 (épaule droite, coude bas)
thetas = ikine_anthroRRR(Wdes,"rd");
disp("3: Sol rd:")
disp(thetas);
% cette configuration doit avoir theta1 proche de pi, theta3 < 0 (épaule gauche, coude haut)
thetas = ikine_anthroRRR(Wdes,"lu");
disp("3: Sol lu:")
disp(thetas);
% cette configuration doit avoir theta1 proche de pi, theta3 > 0 (épaule gauche, coude bas)
thetas = ikine_anthroRRR(Wdes,"ld");
disp("3: Sol ld:")
disp(thetas);


%% Vérification de ikine_anthroRRR par cinématique directe
L1=l1; 
L2=l2; 
L3=l3; 
shoulderoffset=d1;

thetas=rand(1,3)*2*pi-pi;  % validation sur des configurations aléatoires
T=fkine_anthroRRR(thetas,L1, L2, L3, shoulderoffset);  % pas demandé mais conseillé d'implémenter une telle fonction pour valider
thetas1=ikine_anthroRRR(T(1:3,4),"ru");
thetas2=ikine_anthroRRR(T(1:3,4),"rd");
thetas3=ikine_anthroRRR(T(1:3,4),"lu");
thetas4=ikine_anthroRRR(T(1:3,4),"ld");

disp("--------------------")
disp("Original variables")
disp(thetas)
disp("Sol ru:")
disp(thetas1)
disp("Sol rd:")
disp(thetas2)
disp("Sol lu:")
disp(thetas3)
disp("Sol ld:")
disp(thetas4)




%% Cinématique inverse complète - modèle simplifié
%% ikine_gen3lite_simplifie
testconfig = rand(1,6)*2*pi-pi;
%testconfig = zeros(1,6);
%testconfig = [-0.331965  -2.807693   0.038288   0.274872  -1.103367  -2.426876];

% il est conseillé d'implémenter cette fonction fkine de cin. directe,
% soit en utilisant votre code du TP2, soit avec la robotics toolbox
testpose = double(fkine_gen3lite_simplifie(testconfig));  

qvars = zeros(8,6);
qvars(1,:) = ikine_gen3lite_simplifie(testpose, "ru", 0);
qvars(2,:) = ikine_gen3lite_simplifie(testpose, "rd", 0);
qvars(3,:) = ikine_gen3lite_simplifie(testpose, "lu", 0);
qvars(4,:) = ikine_gen3lite_simplifie(testpose, "ld", 0);
qvars(5,:) = ikine_gen3lite_simplifie(testpose, "ru", 1);
qvars(6,:) = ikine_gen3lite_simplifie(testpose, "rd", 1);
qvars(7,:) = ikine_gen3lite_simplifie(testpose, "lu", 1);
qvars(8,:) = ikine_gen3lite_simplifie(testpose, "ld", 1);
% On teste qu'une des 8 configurations est celle dont on est parti
myerror = 10;
for k=1:8
    myerror = min(myerror,norm(testconfig-qvars(k,:)));
end
disp("--------------------")
disp("Cinématique inverse complète - modèle simplifié")
disp("testconfig:")
disp(testconfig)
disp(qvars)
disp("Erreur minimale: ")
disp(myerror)


%% ===========================================
%% =========== QUESTION 8 ====================
%% ===========================================
%% Cinématique inverse complète - modèle exact


if 1

  % Une approche similaire à ci-dessus peut être suivie
  testconfig = rand(1,6)*2*pi-pi;
  testpose = double(fkine_gen3lite(testconfig));
  qvars = zeros(8,6);
  armsoltypes = ["ru"; "rd"; "lu"; "ld"; "ru"; "rd"; "lu"; "ld"];
  wristsolnumbers = [0;0;0;0;1;1;1;1];
  for i = 1:8
%    try
      tmp = ikine_gen3lite_simplifie(testpose, armsoltypes(i,:), wristsolnumbers(i));
      qvars(i,:) = ikine_gen3lite(testpose, tmp(6), armsoltypes(i,:), wristsolnumbers(i));
%    catch
%        warning("Initialise avec theta6=0");     
%        qvars(i,:) = ikine_gen3lite(testpose, 0, armsoltypes(i,:), wristsolnumbers(i));
%    end
  endfor
  % On teste qu'une des 8 configurations est celle dont on est parti
  myerror = 10;
  for k=1:8
      myerror = min(myerror,norm(testconfig-qvars(k,:)));
  end
  disp("--------------------")
  disp("Cinématique inverse complète - modèle original")
  disp("testconfig:")
  disp(testconfig)
  qvars
  disp("Erreur minimale: ")
  disp(myerror)

endif


% Quelques poses à tester en plus

testconfig = [-2.2210, -1.9536, -2.8736, 0.8495, -1.3706, 0.2425];
testpose = [ 0.2700 -0.3889  0.8808 180.1
            -0.9012 -0.4241  0.0890 -68.67
             0.3389 -0.8179 -0.4650 207.8
             0       0       0        1];

testconfig = double(testconfig);
testpose = double(testpose);

qvars = zeros(8,6);
armsoltypes = ["ru"; "rd"; "lu"; "ld"; "ru"; "rd"; "lu"; "ld"];
wristsolnumbers = [0;0;0;0;1;1;1;1];
for i = 1:8
%  try
    tmp = ikine_gen3_simplifie(testpose, armsoltypes(i,:), wristsolnumbers(i));
    qvars(i,:) = ikine_gen3lite(testpose, tmp(6), armsoltypes(i,:), wristsolnumbers(i));
%  catch
%      warning("Essai en initialisant avec theta6 = 0");
%      qvars(i,:) = ikine_gen3lite(testpose, 0, armsoltypes(i,:), wristsolnumbers(i));
%  end
endfor

disp("--------------------")
testconfig
qvars
% On teste qu'une des 8 configurations est celle dont on est parti
myerror = 10;
for k=1:8
    myerror = min(myerror,norm(testconfig-qvars(k,:)));
end
disp("Cinématique inverse complète - modèle original")
disp("Erreur minimale: ")
disp(myerror)

%
%try  
%    % il est aussi possible que le modèle simplifié n'atteigne pas la pose, 
%    % et ikine_gen3lite_simplifie pourrait alors renvoyer une erreur
%    tmp =ikine_gen3lite_simplifie(double(testpose), "ru", 0);
%    qvars(1,:) = ikine_gen3lite(double(testpose), tmp(6), "ru", 0);
%catch
%    % dans ce cas on initialise différemment
%    qvars(1,:) = ikine_gen3lite(double(testpose), 0, "ru", 0);
%end
%try
%    tmp = ikine_gen3lite_simplifie(double(testpose), "rd", 0);
%    qvars(2,:) = ikine_gen3lite(double(testpose), tmp(6), "rd", 0);
%catch
%    warning("Initialise avec theta6=0");
%    qvars(2,:) = ikine_gen3lite(double(testpose), 0, "rd", 0);
%end
%try
%    tmp = ikine_gen3lite_simplifie(double(testpose), "lu", 0);
%    qvars(3,:) = ikine_gen3lite(double(testpose), tmp(6), "lu", 0);
%catch
%    qvars(3,:) = ikine_gen3lite(double(testpose), 0, "lu", 0);
%end
%try 
%    tmp = ikine_gen3lite_simplifie(double(testpose), "ld", 0);
%    qvars(4,:) = ikine_gen3lite(double(testpose), tmp(6), "ld", 0);
%catch
%    qvars(4,:) = ikine_gen3lite(double(testpose), 0, "ld", 0);
%end
%try
%    tmp = ikine_gen3lite_simplifie(double(testpose), "ru", 1);
%    qvars(5,:) = ikine_gen3lite(double(testpose), tmp(6), "ru", 1);
%catch 
%    qvars(5,:) = ikine_gen3lite(double(testpose), 0, "ru", 1);
%end
%try
%    tmp = ikine_gen3lite_simplifie(double(testpose), "rd", 1);
%    qvars(6,:) = ikine_gen3lite(double(testpose), tmp(6), "rd", 1);
%catch 
%    qvars(6,:) = ikine_gen3lite(double(testpose), 0, "rd", 1);
%end
%try
%    tmp = ikine_gen3lite_simplifie(double(testpose), "lu", 1);
%    qvars(7,:) = ikine_gen3lite(double(testpose), tmp(6), "lu", 1);
%catch 
%    qvars(7,:) = ikine_gen3lite(double(testpose), 0, "lu", 1);
%end
%try
%    tmp = ikine_gen3lite_simplifie(double(testpose), "ld", 1);
%    qvars(8,:) = ikine_gen3lite(double(testpose), tmp(6), "ld", 1);
%catch 
%    qvars(8,:) = ikine_gen3lite(double(testpose), 0, "ld", 1);
%end


