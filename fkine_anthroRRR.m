function Ts = fkine_anthroRRR(q_list, L1, L2, L3, shoulderoffset)
  
  d1 = shoulderoffset;
  
  x_s = [1;0;0]; 
  y_s = [0;1;0];
  z_s = [0;0;1];

  %% Repos 
  dx = 0;
  dy = -shoulderoffset;
  dz = L1 + L2 + L3;
  T_repos = [0,1,0,dx; 
             0,0,1,dy; 
             1,0,0,dz; 
             0,0,0,1];
         
  nconfigs = size(q_list,1);
  Ts = zeros(4,4,nconfigs);

  for k = 1:nconfigs
    q = q_list(k,:);

    T_1s = rot( [0; 0; 0],          z_s, q(1));
    T_2s = rot( [0; -d1; L1],      -y_s, q(2));
    T_3s = rot( [0; -d1; L1+L2],    y_s, q(3));
    T_W  = rot( [0; -d1; L1+L2+L3], z_s, 0);
%    T_1s    = rot( [0; 0; 0],        z_s, q(1));
%    T_2s    = rot( [0; 0; 243.3],   -y_s, q(2));
%    T_3s    = rot( [0; -30; 523.3],  y_s, q(3));
%    T_4s    = rot( [0; -10; 523.3],  z_s, q(4));
%    T_5s    = rot( [0; -10; 768.3],  x_s, q(5));
%    T_6s    = rot( [57; -10; 873.3], z_s, q(6));
%    T_tools = rot( [57; -10; 1003.3],z_s, 0);

    T_se_q = T_1s * T_2s * T_3s * T_W * T_repos;
%    T_se_q = T_1s * T_2s * T_3s * T_4s * T_5s * T_6s * T_tools * T_repos;

    Ts(:,:,k) = T_se_q;
  end

end

