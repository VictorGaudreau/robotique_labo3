function [] = matrix2latex(M)

    %% Choose matrix to convert
    % M = rand(4, 5);

    %% Convert
    % Get matrix dimensions
    m = size(M, 1);
    n = size(M, 2);
    % Create first line
    s = sprintf('  \\begin{bmatrix}\n  ');
    % Add matrix content
    for k = 1:m
        for l = 1:n
%            s = sprintf('%s %6.3f', s, M(k, l)); % print 3 decimal places, align to 6 characters
            txt = char(M(k, l));

            s = sprintf('%s %s', s, txt ); % print
            if l < n
                s = sprintf('%s &', s);
            end
        end
        if k < m
            s = sprintf('%s \\\\', s);
        end
        s = sprintf('%s\n  ', s);
    end
    % Add last line
    s = sprintf('%s\\end{bmatrix}\n', s);
    
    % Print the result
    s_mod = s;
    s_mod = strrep (s_mod, "cos", "c");
    s_mod = strrep (s_mod, "sin", "s");
    s_mod = strrep (s_mod, "alpha", "\\alpha");
    s_mod = strrep (s_mod, "beta", "\\beta");
    s_mod = strrep (s_mod, "gamma", "\\gamma");
    s_mod = strrep (s_mod, "*", " \\, ");
    s_mod = strrep (s_mod, "(", "");
    s_mod = strrep (s_mod, ")", "");
               
    disp(s_mod);

end
