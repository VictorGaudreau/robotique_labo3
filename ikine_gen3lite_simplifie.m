function q = ikine_gen3lite_simplifie(T, armsoltype, wristsolnumber)

% Load useful constants
constants

%page 96 chap6
d_we = 130 + 105;
z_sed = T(1:3, 3);
E_sd = T(1:3, 4);

W_s = E_sd - d_we * z_sed;

q_arm = ikine_anthroRRR(W_s, armsoltype);
q1 = q_arm(1);
q2 = q_arm(2);
q3 = q_arm(3);

R_se0 = eye(3);
R_sed = T(1:3, 1:3);

%R_1 = [cos(q1), -sin(q1), 0;
%       sin(q1), cos(q1), 0;
%       0, 0, 1];
%R_2 = [cos(q2), 0, sin(q2);
%       0, 1, 0;
%       -sin(q2), 0, cos(q2)];
%R_3 = [cos(q3), 0, sin(q3);
%       0, 1, 0;
%       -sin(q3), 0, cos(q3)];

T_1s = rot( [0;  0;  0],      z_s, q_arm(1));
T_2s = rot( [0;  d1; l1],    -y_s, q_arm(2));
T_3s = rot( [0; -d1; l2+l2],  y_s, q_arm(3));
R_1 = T_1s(1:3,1:3);
R_2 = T_2s(1:3,1:3);
R_3 = T_3s(1:3,1:3);

%equation 6.7, p96 chap6
R = (R_1*R_2*R_3)' * R_sed * R_se0';

qwrist = dcm2eul(R, wristsolnumber);
q4 = qwrist(1);
q5 = qwrist(2);
q6 = qwrist(3);

q = [q1, q2, q3, q4, q5, q6];

end

