function q = ikine_gen3lite(T, theta6_guess, armsoltype, wristsolnumber)

constants

theta6 = theta6_guess;

x_sed = T(1:3, 1);
y_sed = T(1:3, 2);
z_sed = T(1:3, 3);
E_sd = T(1:3, 4);

MAXITER = 300;
counter = 0;
while true
    W = E_sd + dwo*(sin(theta6)*y_sed - cos(theta6)*x_sed) - dwe*z_sed;
    
    q_arm = ikine_anthroRRR(W, armsoltype);
    q1 = q_arm(1);
    q2 = q_arm(2);
    q3 = q_arm(3);

    R_se0 = eye(3);
    R_sed = T(1:3, 1:3);

    T_1s = rot( [0;  0;  0],      z_s, q_arm(1));
    T_2s = rot( [0;  d1; l1],    -y_s, q_arm(2));
    T_3s = rot( [0; -d1; l2+l2],  y_s, q_arm(3));
    R_1 = T_1s(1:3,1:3);
    R_2 = T_2s(1:3,1:3);
    R_3 = T_3s(1:3,1:3);

    %equation 6.7, p96 chap6
    R = (R_1*R_2*R_3)'*R_sed*R_se0';

    qwrist = dcm2eul(R, wristsolnumber);
    q4 = qwrist(1);
    q5 = qwrist(2);
    q6 = qwrist(3);
    if(abs(q6) - theta6) <= 0.5*pi/180
        q = [q1, q2, q3, q4, q5, q6];
%        disp(counter);
        break;
    elseif counter >= MAXITER
        q = [NaN, NaN, NaN, NaN, NaN, NaN];
%        error("not converging after %d", MAXITER)
        break;
    elseif counter == MAXITER/2
        theta6 = 0;
    else
      theta6 = q6;
    end
    counter = counter + 1;
end


end
